﻿using System;

namespace Webhooks.Consumer
{
	public interface IWebhookSettings
	{
		String SecretToken { get; set; }
	}

	public class DefaultWebhookSettings : IWebhookSettings {
		public DefaultWebhookSettings(String secretToken) {
			this.SecretToken = secretToken;
		}

		public String SecretToken { get; set; }
	}
}

