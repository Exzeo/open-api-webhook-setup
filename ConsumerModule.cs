﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Nancy;
using Nancy.Responses;

namespace Webhooks.Consumer
{
	public class ConsumerModule : NancyModule
	{

		private static int counter = 1;

		public ConsumerModule (IWebhookSettings settings)
		{
			Get ["/"] = parameters => "Nothing to show here";
			Post ["/exzeo_events"] = parameters =>  { 
				counter++;
				return new SampleJsonProcessor (this, settings).Handle (); 
			};
			Get ["/counter"] = p => counter.ToString ();
			Head ["/exzeo_events"] = p => HttpStatusCode.OK;
		}

	}
}
