﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Nancy;
using Nancy.Responses;
using Newtonsoft.Json;

namespace Webhooks.Consumer
{
	class SampleJsonProcessor
	{
		private readonly NancyModule _module;

		// This secret token is harded code for demonstration purposes,
		// ideally, you need to keep it as some environment or configuration
		private String HardcodedSecretToken = String.Empty;

		private readonly DefaultJsonSerializer serializer = new DefaultJsonSerializer ();

		public SampleJsonProcessor (NancyModule module, IWebhookSettings settings)
		{
			_module = module;
			this.HardcodedSecretToken = settings.SecretToken;
		}

		public Response Handle ()
		{

			var json = String.Empty;

			// 1. Ideally, we want to check whether X-Exzeo-EventHash header is present in the 
			//    request, if not, don't continue with the response.
			var headerValue = _module.Request.Headers ["X-Exzeo-EventHash"].FirstOrDefault ();

			if (headerValue == null) {
				return new TextResponse ("Header 'X-Exzeo-EventHash' is missing").WithStatusCode (HttpStatusCode.NotAcceptable);
			}


			// 2. Read the value body as a string
			using (StreamReader reader = new StreamReader (_module.Request.Body)) {
				json = reader.ReadToEnd ();
			}

			if (EnsureAuthenticatedPost (headerValue, json, HardcodedSecretToken)) {
				return new JsonResponse (new { status = "OK" }, serializer);
			}

			return new TextResponse ("No Match").WithStatusCode (HttpStatusCode.Unauthorized);
		}

		/// <summary>
		/// Generates a HMA SHA1 hexdigest for a given string using a salt
		/// </summary>
		/// <param name="value">The value that needs to be hashed</param>
		/// <param name="salt">Salt to generte uniqueness, ideally a user provided value</param>
		/// <returns></returns>
		private byte[] GetMessageHash (string value, string salt)
		{
			var tokenInBytes = Encoding.UTF8.GetBytes (salt);
			var hashValue = new byte[0];

			using (HMACSHA1 sha1 = new HMACSHA1 (tokenInBytes)) {
				hashValue = sha1.ComputeHash (Encoding.UTF8.GetBytes (value));
			}
			// Make sure we convert the hash value to a hex string before converting it to byte array
			return Encoding.UTF8.GetBytes (ToHexString (hashValue));
		}

		/// <summary>
		/// Generates a hexadecimal formatted string for the given byte array
		/// </summary>
		/// <param name="array">Array of bytes</param>
		/// <returns>Hex encoded string</returns>
		public static string ToHexString (byte[] array)
		{
			StringBuilder hex = new StringBuilder (array.Length * 2);
			foreach (byte b in array) {
				hex.AppendFormat ("{0:x2}", b);
			}
			return hex.ToString ();
		}

		/// <summary>
		/// Verifies the inbound JSON message is a valid one or not
		/// </summary>
		/// <param name="inboundHash">Hash value sent through the HTTP header</param>
		/// <param name="inboundJson">JSON body HTTP posted</param>
		/// <param name="secretToken">Secret token set by the webhook</param>
		/// <returns>true, if match</returns>
		public bool EnsureAuthenticatedPost (String inboundHash, String inboundJson, String secretToken)
		{
			// Convert hash value coming in header as a bytes
			var hashValue = Encoding.UTF8.GetBytes (inboundHash);
			// Compute the hash for body with secret token as a salt
			var computedHash = GetMessageHash (inboundJson, HardcodedSecretToken);

			var result = hashValue.SequenceEqual (computedHash);

			Console.WriteLine ("Json: \r\n" + JsonPrettify(inboundJson));
			Console.WriteLine ();
			Console.WriteLine ("hashValue :" + ToHexString (hashValue));
			Console.WriteLine ("computedValue :" + ToHexString (computedHash));
			Console.WriteLine ("is_matching :" + result);

			return result;
		}
			
		private static String JsonPrettify (string json)
		{
			using (var stringReader = new StringReader (json))
			using (var stringWriter = new StringWriter ())
			using (var jsonReader = new JsonTextReader (stringReader))
			using (var jsonWriter = new JsonTextWriter (stringWriter) { Formatting = Formatting.Indented }) {
				jsonWriter.WriteToken (jsonReader);
				return stringWriter.ToString ();
			}
		}
	}
}
